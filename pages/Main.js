/* eslint-disable react-native/no-inline-styles */
/*Screen to register the user*/
import React from 'react';
import {
  View,
  ScrollView,
  FlatList,
  KeyboardAvoidingView,
  Alert,
  Text,
} from 'react-native';
import Mytextinput from './components/Mytextinput';
import Mybutton from './components/Mybutton';
import Mytext from './components/Mytext';
import {openDatabase} from 'react-native-sqlite-storage';
var db = openDatabase({name: 'UserDatabase.db'});
export default class RegisterUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_name: '',
      user_contact: '',
      user_address: '',
      FlatListItems: [],
    };
    this.navigation = this.props.navigation;
    db.transaction(function(txn) {
      txn.executeSql(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='table_user'",
        [],
        function(tx, res) {
          console.log('item:', res.rows.length);
          if (res.rows.length == 0) {
            txn.executeSql('DROP TABLE IF EXISTS table_user', []);
            txn.executeSql(
              'CREATE TABLE IF NOT EXISTS table_user(user_id INTEGER PRIMARY KEY AUTOINCREMENT, user_name VARCHAR(20), user_contact INT(10), user_address VARCHAR(255))',
              [],
            );
          }
        },
      );
    });
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM table_user', [], (tx, results) => {
        var temp = [];
        for (let i = 0; i < results.rows.length; ++i) {
          temp.push(results.rows.item(i));
        }
        this.setState({
          FlatListItems: temp,
        });
      });
    });
  }

  componentDidMount() {
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM table_user', [], (tx, results) => {
        var temp = [];
        for (let i = 0; i < results.rows.length; ++i) {
          temp.push(results.rows.item(i));
        }
        this.setState({
          FlatListItems: temp,
        });
      });
    });
    this.navigation.addListener('willFocus', () => {
      db.transaction(tx => {
        tx.executeSql('SELECT * FROM table_user', [], (tx, results) => {
          var temp = [];
          for (let i = 0; i < results.rows.length; ++i) {
            temp.push(results.rows.item(i));
          }
          this.setState({
            FlatListItems: temp,
          });
        });
      });
    });
  }
  register_user = () => {
    var that = this;
    const {user_name} = this.state;
    const {user_contact} = this.state;
    const {user_address} = this.state;
    //alert(user_name, user_contact, user_address);
    if (user_name) {
      if (user_contact) {
        if (user_address) {
          if (+user_contact < 1 || +user_contact > 100) {
            console.log('!!!!!!!!!!!!');
            return alert('Age Failed');
          }

          db.transaction(function(tx) {
            tx.executeSql(
              'INSERT INTO table_user (user_name, user_contact, user_address) VALUES (?,?,?)',
              [user_name, user_contact, user_address],
              (tx, results) => {
                console.log('Results', results.rowsAffected);
                if (results.rowsAffected > 0) {
                  Alert.alert(
                    'Success',
                    'You are Registered Successfully',
                    [
                      {
                        text: 'Ok',
                        onPress: () =>
                          that.props.navigation.navigate('MainScreen'),
                      },
                    ],
                    {cancelable: false},
                  );
                } else {
                  alert('Registration Failed');
                }
              },
            );
          });
        } else {
          alert('Please fill Address');
        }
      } else {
        alert('Please fill Contact Number');
      }
    } else {
      alert('Please fill Name');
    }
  };
  ListViewItemSeparator = () => {
    return (
      <View style={{height: 0.2, width: '100%', backgroundColor: '#808080'}} />
    );
  };
  render() {
    return (
      <View style={{backgroundColor: 'white', flex: 1}}>
        <Mytext text="          THÔNG TIN SINH VIÊN      " />
        <ScrollView
          keyboardShouldPersistTaps="handled"
          style={{
            height: '50%',
          }}>
          <KeyboardAvoidingView
            behavior="padding"
            style={{flex: 1, justifyContent: 'space-between'}}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Mytext text=" Tên      " />
              <Mytextinput
                placeholder="Enter Name                            "
                onChangeText={user_name => this.setState({user_name})}
                style={{padding: 10}}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Mytext text="Tuổi     " />
              <Mytextinput
                placeholder="Enter Age                                 "
                onChangeText={user_contact => this.setState({user_contact})}
                maxLength={10}
                keyboardType="numeric"
                style={{padding: 10}}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Mytext text="Địa chỉ" />
              <Mytextinput
                placeholder="Enter Address                           "
                onChangeText={user_address => this.setState({user_address})}
                maxLength={225}
                numberOfLines={1}
                multiline={true}
                style={{textAlignVertical: 'top', padding: 10}}
              />
            </View>
            <View>
              <Mybutton title="+" customClick={this.register_user.bind(this)} />
            </View>
          </KeyboardAvoidingView>
        </ScrollView>

        <FlatList
          style={{
            height: '50%',
          }}
          data={this.state.FlatListItems}
          ItemSeparatorComponent={this.ListViewItemSeparator}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => (
            <View
              key={item.user_id}
              style={{backgroundColor: 'white', padding: 20}}>
              {/* <Text>Id: {item.user_id}</Text> */}
              <Text>{item.user_name}</Text>
              <Text>Tuổi: {item.user_contact}</Text>
              <Text>Địa chỉ: {item.user_address}</Text>
            </View>
          )}
        />
      </View>
    );
  }
}
