/* eslint-disable react-native/no-inline-styles */
/*Custom TextInput*/
import React from 'react';
import {View, TextInput} from 'react-native';
const Mytextinput = props => {
  return (
    <View
      style={{
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        borderBottomColor: '#007FFF',
        borderBottomWidth: 1,
      }}>
      <TextInput
        underlineColorAndroid="transparent"
        placeholder={props.placeholder}
        placeholderTextColor="#007FFF"
        keyboardType={props.keyboardType}
        onChangeText={props.onChangeText}
        returnKeyType={props.returnKeyType}
        numberOfLines={props.numberOfLines}
        multiline={props.multiline}
        onSubmitEditing={props.onSubmitEditing}
        style={props.style}
        blurOnSubmit={false}
        value={props.value}
      />
    </View>
  );
};
export default Mytextinput;
