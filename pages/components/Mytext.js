/*Custom Text*/
import React from 'react';
import {TouchableHighlight, Text, StyleSheet} from 'react-native';

const Mytext = props => {
  return <Text style={styles.text}>{props.text}</Text>;
};
const styles = StyleSheet.create({
  text: {
    // color: '#111825',
    color: '#007FFF',
    fontWeight: 'bold',
    fontSize: 16,
    marginTop: 15,
    marginLeft: 10,
  },
});
export default Mytext;
